import { useEffect, useState } from "react";
import axios from "axios";

export default function useFetchData() {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const COUNTRIES_API_URL = `https://restcountries.eu/rest/v2/all`;
      try {
        const data = await axios.get(COUNTRIES_API_URL);
        setData(data.data);
        setIsLoading(false);
      } catch (error) {
        setError(error);
      }
    };
    fetchData();
  }, []);

  return { data, isLoading, error };
}
