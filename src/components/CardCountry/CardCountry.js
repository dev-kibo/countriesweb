import React from "react";
import CardCountryImg from "./CardCountryImg";
import CardCountryBody from "./CardCountryBody";
import CardCountryFooter from "./CardCountryFooter";

export default function CardCountry({ country }) {
  return (
    <div className="flex flex-row lg:flex-row md:flex-col sm:flex-col xs:flex-col xs:w-auto w-4/12 lg:w-4/12 md:w-6/12 sm:w-5/12 h-auto border-l-8 border-blue-300 shadow-md bg-white">
      <CardCountryImg flag={country.flag} />
      <CardCountryBody country={country} />
      <CardCountryFooter languages={country.languages} />
    </div>
  );
}
