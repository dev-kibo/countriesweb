import React from "react";

export default function CardCountryFooter({ languages }) {
  return (
    <div className="flex flex-col items-start ml-4 pt-2 p-4">
      <p className="font-bold text-gray-900">Speaking</p>
      <ul className="list-none pl-2">
        {languages.map(lang => {
          return (
            <li key={lang.name} className="text-gray-700">
              {lang.name}
            </li>
          );
        })}
      </ul>
    </div>
  );
}
