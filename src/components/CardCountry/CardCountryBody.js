import React from "react";

export default function CardCountryBody({ country }) {
  const formatedPopulation = Number(country.population).toLocaleString();

  return (
    <div className="flex flex-col justify-between p-2">
      <p className="text-sm font-bold text-gray-900">{country.region}</p>
      <p className="text-2xl text-indigo-900">{country.name}</p>
      <p className="text-normal text-gray-700">
        Capital is <span className="font-bold">{country.capital}</span>
      </p>
      <p className="text-gray-700">Population of {formatedPopulation}</p>
    </div>
  );
}
