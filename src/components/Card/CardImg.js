import React from "react";

export default function CardImg({ flag }) {
  return (
    <div>
      <img className="object-contain" src={flag} alt="Country flag" />
    </div>
  );
}
