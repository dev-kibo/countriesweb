import React from "react";

export default function CardBody({ data }) {
  // Formating population number with right decimal places
  const formatedPopulation = Number(data.population).toLocaleString();
  // Styles are the same for all paragraphs
  const pStyle = "font-bold text-gray-700";

  return (
    <div className="flex-grow flex flex-col justify-end p-2">
      <p>
        In <span className={pStyle}>{data.region}</span>
      </p>
      <p>
        Capital is <span className={pStyle}>{data.capital}</span>
      </p>
      <p>
        Population of <span className={pStyle}>{formatedPopulation}</span>
      </p>
    </div>
  );
}
