import React from "react";
import CardTitle from "./CardTitle";
import CardImg from "./CardImg";
import CardBody from "./CardBody";

export default function Card({ country, handleShowCountry }) {
  return (
    <button
      className="flex flex-col text-left items-stretch w-64 shadow-md m-2 border-l-8 border-blue-300 bg-gray-100 outline-none"
      onClick={handleShowCountry}
      value={country.numericCode}
    >
      <CardTitle name={country.name} />
      <CardImg flag={country.flag} />
      <CardBody data={country} />
    </button>
  );
}
